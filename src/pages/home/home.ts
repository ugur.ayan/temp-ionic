import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public itemsList = [
    {"name":"Mark", "position":"CEO"},
    {"name":"David", "position":"Developer"}
  ];
  constructor(public navCtrl: NavController) {

  }

}
